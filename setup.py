# -*- coding: utf-8 -*-
"""
This module contains the tool of gdf.bandi
"""
import os
from setuptools import setup, find_packages

version = '1.0.5.dev0'

tests_require = ['plone.app.testing', 'rer.bandi']

setup(name='gdf.bandi',
      version=version,
      description="A product for announcements management",
      long_description=open("README.rst").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Plone",
        "Framework :: Plone :: 3.3",
        "Framework :: Plone :: 4.0",
        "Framework :: Plone :: 4.1",
        "Framework :: Plone :: 4.2",
        "Development Status :: 5 - Production/Stable",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        ],
      keywords='gdf rer bandi announcements',
      author='RedTurtle Technology',
      author_email='sviluppoplone@redturtle.it',
      url='https://bitbucket.org/cekk/gdf.bandi',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['gdf', ],
      include_package_data=True,
      zip_safe=False,
      install_requires=['setuptools',
                        'plone.app.registry',
                        'archetypes.schemaextender',
                        'rer.bandi',
                        ],
      tests_require=tests_require,
      extras_require=dict(tests=tests_require),
      entry_points="""
      # -*- entry_points -*-
      [z3c.autoinclude.plugin]
      target = plone
      """,
      )
