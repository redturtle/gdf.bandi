# -*- coding: utf-8 -*-
from DateTime import DateTime
from plone.app.form.widgets.uberselectionwidget import UberSelectionWidget
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from rer.bandi.portlets import collection
from zope import schema
from zope.component import getUtility
from zope.formlib import form
from zope.interface import implements
try:
    from zope.app.schema.vocabulary import IVocabularyFactory
except ImportError:
    from zope.schema.interfaces import IVocabularyFactory


class IGDFBandoCollectionPortlet(collection.IBandoCollectionPortlet):

    """A portlet which renders the results of a collection object.
    """

    show_ente = schema.Bool(
        title=u'Mostra ente',
        required=False,
        default=False
    )

    show_chiusura_procedimento = schema.Bool(
        title=u'Mostra data di chiusura del procedimento ',
        required=False,
        default=False
    )

    css_class = schema.TextLine(
        title=u'Classe css da applicare alla portlet',
        required=False,
        default=u'',
    )


class GDFAssignment(collection.Assignment):

    """
    Portlet assignment.
    This is what is actually managed through the portlets UI and associated
    with columns.
    """

    implements(IGDFBandoCollectionPortlet)

    css_class = u''

    def __init__(
        self, header=u"",
        target_collection=None,
        limit=None,
        show_more=True,
        show_more_text=None,
        show_more_path=None,
        show_description=False,
        show_tipologia_bando=False,
        show_effective=False,
        show_scadenza_bando=False,
        show_ente=False,
        show_chiusura_procedimento=False,
        css_class=u''
    ):
        self.header = header
        self.target_collection = target_collection
        self.limit = limit
        self.show_more = show_more
        self.show_more_text = show_more_text
        self.show_more_path = show_more_path
        self.show_description = show_description
        self.show_tipologia_bando = show_tipologia_bando
        self.show_effective = show_effective
        self.show_scadenza_bando = show_scadenza_bando
        self.show_ente = show_ente
        self.show_chiusura_procedimento = show_chiusura_procedimento
        self.css_class = css_class


class GDFRenderer(collection.Renderer):

    """Portlet renderer.
    """

    _template = ViewPageTemplateFile('gdf_collection.pt')
    render = _template

    def __init__(self, *args):
        collection.Renderer.__init__(self, *args)
        self.voc_tipologia = getUtility(
            IVocabularyFactory,
            name='gdf.bandi.alternative_types.vocabulary'
        )(self.context)

    def show_scadenza_bando(self, bando):
        ''' Condition to show the scadenza bando field
         - the checkbox show_scadenza_bando is checked in the portlet
           configuration
         - the field scadenza_bando contains a past DateTime instance
        '''
        if not self.data.show_scadenza_bando:
            return False
        scadenza_bando = bando.getScadenza_bando()
        if not isinstance(scadenza_bando, DateTime):
            return False
        if scadenza_bando.isPast():
            return False
        return True


class GDFAddForm(collection.AddForm):

    """Portlet add form
    """

    form_fields = form.Fields(IGDFBandoCollectionPortlet)
    form_fields['target_collection'].custom_widget = UberSelectionWidget
    form_fields['show_more_path'].custom_widget = UberSelectionWidget

    def create(self, data):
        return GDFAssignment(**data)


class GDFEditForm(collection.EditForm):

    """Portlet edit form
    """

    form_fields = form.Fields(IGDFBandoCollectionPortlet)
    form_fields['target_collection'].custom_widget = UberSelectionWidget
    form_fields['show_more_path'].custom_widget = UberSelectionWidget

    def create(self, data):
        return GDFAssignment(**data)
