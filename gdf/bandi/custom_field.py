# -*- coding: utf-8 -*-

from zope import schema
from zope.interface import Interface, implements

from z3c.form.object import registerFactoryAdapter
from gdf.bandi import gdfbandiMessageFactory as _
from plone.registry.field import PersistentField


class IBandiTypesField(Interface):
    type_id = schema.ASCIILine(title=_(u"Typology id"), required=True)
    type_title = schema.ASCIILine(title=_(u"Typology title"), required=True)


class BandiTypesField(object):
    implements(IBandiTypesField)


class PersistentObject(PersistentField, schema.Object):
    pass

registerFactoryAdapter(IBandiTypesField, BandiTypesField)
