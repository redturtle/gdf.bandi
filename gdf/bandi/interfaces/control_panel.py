from zope import schema
from zope.interface import Interface
from gdf.bandi import gdfbandiMessageFactory as _
from gdf.bandi.custom_field import PersistentObject, IBandiTypesField


class IGDFBandiSettings(Interface):
    """Global GDF Bandi settings. This describes records stored in the
    configuration registry and obtainable via plone.registry.
    """

    other_recipient = schema.Tuple(
            title=_(u"Other recipients list"),
            description=_("help_other_recipients_registry",
                          default=u'Defines a list of other recipients'),
            required=False,
            value_type=schema.TextLine(),
            default=(u'',))

    alternative_types = schema.Tuple(
            title=_(u'Announcements typologies'),
            description=_('help_tabs_mapping',
                          default=u"Insert a list of typologies for announcements. These values overrides default vocabulary from rer.bandi."),
            value_type=PersistentObject(IBandiTypesField, title=_(u"Bando type")),
            required=False,
            default=(),
            missing_value=()
        )

    category = schema.Tuple(
            title=_(u"Categories list"),
            description=_("help_category_registry",
                          default=u'Defines a list of categories'),
            required=False,
            value_type=schema.TextLine(),
            default=(u'',))