from zope.i18nmessageid import MessageFactory
gdfbandiMessageFactory = MessageFactory('gdf.bandi')
import logging
logger = logging.getLogger('gdf.bandi')


def initialize(context):
    """Initializer called when used as a Zope 2 product."""
