try:
    from zope.app.schema.vocabulary import IVocabularyFactory
except ImportError:
    from zope.schema.interfaces import IVocabularyFactory
from zope.interface import implements
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from zope.component import queryUtility
from plone.registry.interfaces import IRegistry
from gdf.bandi.interfaces import IGDFBandiSettings


class OtherRecipientVocabulary(object):

    """
    A simple vocabulary taken from the registry
    """
    implements(IVocabularyFactory)

    def __call__(self, context):
        context = getattr(context, 'context', context)
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IGDFBandiSettings, check=False)
        if settings:
            recipients = getattr(settings, 'other_recipient', ())
        else:
            recipients = ()

        recipients = recipients or ()
        return SimpleVocabulary([SimpleTerm(value=value, token=value, title=value)
                                 for value in recipients])

OtherRecipientVocabularyFactory = OtherRecipientVocabulary()


class CategoryVocabulary(object):

    """
    A simple vocabulary taken from the registry
    """
    implements(IVocabularyFactory)

    def __call__(self, context):
        context = getattr(context, 'context', context)
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IGDFBandiSettings, check=False)
        if settings:
            categories = getattr(settings, 'category', ())
        else:
            categories = ()

        categories = categories or ()
        return SimpleVocabulary([SimpleTerm(value=value, token=value, title=value)
                                 for value in categories])

CategoryVocabularyFactory = CategoryVocabulary()


class BandiTypesAlternativeVocabulary(object):

    """
    A simple vocabulary taken from the registry
    """
    implements(IVocabularyFactory)

    def __call__(self, context):
        context = getattr(context, 'context', context)
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IGDFBandiSettings, check=False)
        if settings:
            types = getattr(settings, 'alternative_types', ())
            return SimpleVocabulary([SimpleTerm(value=x.type_id, token=x.type_id, title=x.type_title)
                                     for x in types])
        else:
            return SimpleVocabulary.fromItems([])

BandiTypesAlternativeVocabularyFactory = BandiTypesAlternativeVocabulary()
