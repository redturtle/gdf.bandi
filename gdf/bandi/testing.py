# -*- coding: utf-8 -*-

from zope.configuration import xmlconfig

from plone.testing import z2

from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting
from plone.app.testing import applyProfile
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID


class GDFBandi(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE, )

    def setUpZope(self, app, configurationContext):
        # Load ZCML for this package
        import gdf.bandi
        xmlconfig.file('configure.zcml',
                       gdf.bandi,
                       context=configurationContext)
        z2.installProduct(app, 'gdf.bandi')

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'gdf.bandi:default')
        #quickInstallProduct(portal, 'collective.analyticspanel')
        setRoles(portal, TEST_USER_ID, ['Member', 'Manager'])


GDF_BANDI_FIXTURE = GDFBandi()
GDF_BANDI_INTEGRATION_TESTING = \
    IntegrationTesting(bases=(GDF_BANDI_FIXTURE, ),
                       name="AnalyticsPanel:Integration")
GDF_BANDI_FUNCTIONAL_TESTING = \
    FunctionalTesting(bases=(GDF_BANDI_FIXTURE, ),
                       name="AnalyticsPanel:Functional")
