# -*- coding: utf-8 -*-
from Products.statusmessages.interfaces import IStatusMessage
from plone.app.registry.browser import controlpanel
from z3c.form import button
from z3c.form import group
from z3c.form import field
from gdf.bandi.interfaces import IGDFBandiSettings
from gdf.bandi import gdfbandiMessageFactory as _


def fix_widget_style(widget):
    widget.style = u'width: 100%';
    widget.klass += u" autoresize";
    widget.rows = 7


class GDFBandiSettingsEditForm(controlpanel.RegistryEditForm):
    """Settings form
    """
    schema = IGDFBandiSettings
    id = "GDFBandiSettingsEditForm"
    label = _(u"GDF Bandi settings")
    description = _(u"help_gdf_bandi_settings_editform",
                    default=u"Manage settings for GDF bandi")

    @button.buttonAndHandler(_('Save'), name='save')
    def handleSave(self, action):
        data, errors = self.extractData()
        if errors:
            self.status = self.formErrorsMessage
            return
        changes = self.applyChanges(data)
        IStatusMessage(self.request).addStatusMessage(_(u"Changes saved"),
                                                      "info")
        self.context.REQUEST.RESPONSE.redirect("@@gdf-bandi-settings")

    @button.buttonAndHandler(_('Cancel'), name='cancel')
    def handleCancel(self, action):
        IStatusMessage(self.request).addStatusMessage(_(u"Edit cancelled"),
                                                      "info")
        self.request.response.redirect("%s/%s" % (self.context.absolute_url(),
                                                  self.control_panel_view))

    def updateWidgets(self):
        super(GDFBandiSettingsEditForm, self).updateWidgets()
        fix_widget_style(self.widgets['other_recipient'])


class GDFBandiSettingsControlPanel(controlpanel.ControlPanelFormWrapper):
    """
    Analytics settings control panel.
    """
    form = GDFBandiSettingsEditForm
    #index = ViewPageTemplateFile('controlpanel.pt')
