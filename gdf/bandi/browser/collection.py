# -*- coding: utf-8 -*-
from rer.bandi.browser.collection import CollectionBandiView
from zope.component import getUtility
try:
    from zope.app.schema.vocabulary import IVocabularyFactory
except ImportError:
    from zope.schema.interfaces import IVocabularyFactory


class View(CollectionBandiView):

    show_tipologia = True

    def __init__(self, context, request):
        '''
        We need a custom voc_tipologia
        '''
        self.context = context
        self.request = request
        self.voc_tipologia = getUtility(
            IVocabularyFactory,
            name='gdf.bandi.alternative_types.vocabulary'
        )(self.context)

    def getFieldValue(self, item, field_id):
        """
        return the value of given field_id
        """
        obj = item.getObject()
        field = obj.Schema().get(field_id, None)
        if field:
            return field.get(obj)
        return ''


class ViewNoPublishedOn(View):
    ''' #262 New view that omits the published_on field
    '''

    hide_published_on = True
