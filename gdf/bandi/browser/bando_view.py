# -*- coding: utf-8 -*-
from .. import gdfbandiMessageFactory as _
from rer.bandi import bandiMessageFactory as _rer
from rer.bandi.browser.bando import BandoView
from rer.bandi.interfaces import IBandoFolderDeepening
try:
    from zope.app.schema.vocabulary import IVocabularyFactory
except ImportError:
    from zope.schema.interfaces import IVocabularyFactory

from zope.component import getUtility
from zope.i18n import translate


class View(BandoView):

    """
    custom view for GDF Bandi
    """

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.voc_tipologia = getUtility(
            IVocabularyFactory,
            name='gdf.bandi.alternative_types.vocabulary'
        )(self.context)
        self.voc_categoria = getUtility(
            IVocabularyFactory,
            name='gdf.bandi.category.vocabulary'
        )(self.context)

    def getFieldValue(self, field_id):
        """
        return the value of given field_id
        """
        field = self.context.Schema().get(field_id, None)
        if field:
            return field.get(self.context)
        return ''

    def getBandoType(self):
        """
        get bando type title from vocabulary
        """
        try:
            return self.voc_tipologia.getTermByToken(
                self.context.tipologia_bando
            ).title
        except:
            return self.context.tipologia_bando

    def getBandoCategory(self):
        """
        get bando category title from vocabulary
        """
        try:
            return self.voc_categoria.getTermByToken(
                self.context.bandiCategory
            ).title
        except:
            return ""

    def getBandoState(self):
        """
        return corretc bando state
        """
        scadenza_bando = self.context.scadenza_bando
        chiusura_procedimento_bando = self.context.chiusura_procedimento_bando
        state = ('open', translate(_rer(u'Open'), context=self.request))
        if scadenza_bando and scadenza_bando.isPast():
            if (
                chiusura_procedimento_bando
                and chiusura_procedimento_bando.isPast()
            ):
                state = (
                    'closed',
                    translate(_rer(u'Closed'), context=self.request)
                )
            else:
                state = (
                    'inProgress',
                    translate(_rer(u'In progress'), context=self.request)
                )
        elif (
            chiusura_procedimento_bando
            and chiusura_procedimento_bando.isPast()
        ):
            state = (
                'closed',
                translate(_rer(u'Closed'), context=self.request)
            )
        return state

    def retrieveFolderDeepening(self):
        """Retrieves all Folder Deepening objects
        contained in Structured Document
        """
        struct_doc = self.context
        values = []
        dfolders = struct_doc.getFolderContents(
            contentFilter={
                'object_provides': IBandoFolderDeepening.__identifier__
                }
            )
        for df in dfolders:
            if not df.exclude_from_nav:
                published_date = df.effective.Date()
                if published_date == '1000/01/01':
                    published_date = ''
                values.append(dict(title=df.Title,
                                   description=df.Description,
                                   url=df.getURL(),
                                   path=df.getPath(),
                                   pub_date=published_date,
                                   id=df.getId,
                                   ))
        return values
