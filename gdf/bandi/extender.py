from Products.Archetypes.atapi import DateTimeField, StringField, \
    SelectionWidget, CalendarWidget, StringWidget
from archetypes.schemaextender.field import ExtensionField
from archetypes.schemaextender.interfaces import IBrowserLayerAwareExtender, \
    IOrderableSchemaExtender, ISchemaModifier
from gdf.bandi import gdfbandiMessageFactory as _
from gdf.bandi.interfaces import IGDFBandiLayer
from rer.bandi.interfaces import IBando
from zope.component import adapts
from zope.interface import implements


class BandiStringField(ExtensionField, StringField):
    """Extension field for Text"""


class BandiDateTimeField(ExtensionField, DateTimeField):
    """Extension field for Text"""


class GDFBandiExtender(object):

    adapts(IBando)

    implements(IOrderableSchemaExtender, ISchemaModifier, IBrowserLayerAwareExtender)
    layer = IGDFBandiLayer

    fields = [BandiStringField(name='bandiCodiceIdentificativoGara',
                          searchable=True,
                          widget=StringWidget(label=_(u"Tender code"),
                                  description=_(u'help_bandi_other_cig',
                                                default=u'C.I.G code'))
                          ),
              BandiStringField(name='bandiOtherRecipient',
                             vocabulary_factory='gdf.bandi.other_recipient.vocabulary',
                             widget=SelectionWidget(
                                 label=_(u'label_bandi_other_recipient',
                                            default=u'Other Recipient'),
                                 description=_(u'help_bandi_other_recipient',
                                                default=u'Select another recipient.'),
                               )),
              BandiDateTimeField(name='bandiGURIPublication',
                             widget=CalendarWidget(
                                label=_('label_bandi_guri_publication', default=u"GURI publication date"),
                                description=_('help_bandi_guri_publication', default=u''),
                                show_hm=False,
                                ),
                             validators=('isValidDate')),
              BandiDateTimeField(name='bandiGUUEPublication',
                             widget=CalendarWidget(
                                label=_('label_bandi_guue_publication', default=u"GUUE publication date"),
                                description=_('help_bandi_guue_publication', default=u''),
                                show_hm=False,
                                ),
                             validators=('isValidDate')),
              BandiStringField(name='bandiCategory',
                             vocabulary_factory='gdf.bandi.category.vocabulary',
                             default="",
                             widget=SelectionWidget(
                                 label=_(u'label_bandi_categories',
                                            default=u'Category'),
                                 description=_(u'help_bandi_category',
                                                default=u'Select a category.'),
                               )),
              ]

    def __init__(self, context):
        self.context = context

    def getFields(self):
        return self.fields

    def getOrder(self, original):
        """
        fix fields order
        """
        original['default'] = ['id', 'title', 'description', 'constrainTypesMode', 'locallyAllowedTypes', 'immediatelyAddableTypes', 'text', 'tipologia_bando', 'bandiCategory', 'destinatari', 'bandiOtherRecipient',
                               'ente_bando', 'bandiCodiceIdentificativoGara', 'scadenza_bando', 'chiusura_procedimento_bando',
                               'bandiGURIPublication', 'bandiGUUEPublication', 'riferimenti_bando']
        return original

    def fiddle(self, schema):
        """
        inherited method from ISchemaModifier that allows to modify existent schema
        """
        schema['tipologia_bando'].vocabulary_factory = 'gdf.bandi.alternative_types.vocabulary'
