# -*- coding: utf-8 -*-
from Products.CMFCore.utils import getToolByName
import logging


PROFILE_ID = 'profile-gdf.bandi:default'


def update_gdf_bandi(context, logger=None):
    """Performs the profile update
    """
    if logger is None:
        # Called as upgrade step: define our own logger.
        logger = logging.getLogger('gdf.bandi')

    setup = getToolByName(context, 'portal_setup')
    setup.runImportStepFromProfile(PROFILE_ID, 'portlets')

    logger.info("Profile updated.")
