# -*- coding: utf-8 -*-
from Products.CMFCore.utils import getToolByName
import logging


PROFILE_ID = 'profile-gdf.bandi:default'


def update_gdf_bandi(context, logger=None):
    """Performs the profile update
    """
    if logger is None:
        logger = logging.getLogger('gdf.bandi')

    setup = getToolByName(context, 'portal_setup')
    setup.runImportStepFromProfile(PROFILE_ID, 'typeinfo')

    logger.info("Profile updated.")
