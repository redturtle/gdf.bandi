# -*- coding: utf-8 -*-
from zope.component import queryUtility, getMultiAdapter
from plone.registry.interfaces import IRegistry
from gdf.bandi.interfaces import IGDFBandiSettings
from gdf.bandi.testing import GDF_BANDI_INTEGRATION_TESTING
from base import BaseTestCase


class TestConfiguration(BaseTestCase):

    layer = GDF_BANDI_INTEGRATION_TESTING

    def test_default_configuration(self):
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IGDFBandiSettings, check=False)
        self.assertEqual(settings.other_recipient, [])

    def test_bandi_controlpanel_view(self):
        """
        test if the configuration panel is accesible
        """
        # portal = self.layer['portal']
        # request = self.layer['request']
        # self.markRequestWithLayer()
        view = getMultiAdapter((self.portal, self.portal.REQUEST),
                               name="gdf-bandi-settings")
        view = view.__of__(self.portal)
        self.failUnless(view())

    def test_bandi_controlpanel_view_protected(self):
        """
        test that the control panel is protected and anonymous can't access
        """
        from AccessControl import Unauthorized
        self.logout()
        self.assertRaises(Unauthorized,
                          self.portal.restrictedTraverse,
                         '@@gdf-bandi-settings')
